from setuptools import setup, find_packages
from glob import glob

scripts = glob("scripts/*")
scripts = [script for script in scripts if script[-1] != '~']

setup(name='pyttmult',
      use_scm_version=True,
      setup_requires=['setuptools_scm'],
      description='Python interface to TTmult CTM codes',
      author='Charles Titus',
      author_email='ctitus@stanford.edu',
      license='MIT',
      packages=find_packages(),
      scripts=scripts,
      classifiers=['Programing Language :: Python :: 2.7',],
      install_requires=['numpy', 'fire']
)
