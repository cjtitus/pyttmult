from __future__ import print_function
from os.path import exists, join
from shutil import copy, move
from os import remove, getenv, devnull, system, name
from subprocess import Popen

"""
Canonical file endings are going to be 
.rcg
.m14
.m15
.org
.ora
.oba
.ban
"""
ismac = False
isunix = True
ispc = False
if name=='nt':
    ispc=True
    isunix=False
if name=='posix':
    isunix=True
    ispc=False
    #There is no support for os.link for pc before python v.3.2
    from os import link
def cat(*args):
    return ''.join(args)
def getBin():
    if ispc:
        binpath='c:\\cowan\\bin\\'
    if isunix:
        binpath=join(getenv('ttmult'),'bin')
    return binpath
def ttmult(filename, calctype='all', bandertype='lanczos', verbose=False, binpath=None):
    """
    bandertype = 'exact' or 'lanczos'
    calctype = 'all' or just the 'bander' part of the calculation or 'rcg_racer'
    for just the rcg and racer part of calcultation.
    Linux users need to set their bin path as a enviroment variable 'ttmult'
    Michael L. Baker, Charles Titus
    """
    if binpath is None:
        binpath=getBin()
    if not exists(filename + '.m14') or not exists(filename + '.m15'):
        if calctype == 'bander':
            if verbose:
                print('m14 and m15 files are not present; running full calc')
                calctype='all'
    if calctype == 'all':
        if verbose:
            print('Executing ttrcg, ttrac and ttban (',bandertype,')')
        runrcg(filename, verbose=verbose, binpath=binpath)
        runrac(filename,verbose=verbose, binpath=binpath)
        runban(filename, bandertype, verbose=verbose, binpath=binpath)
    elif calctype == 'rcg_racer' or calctype == 'rcg_rac':
        if verbose:
            print('Executing ttrcg and ttrac only')
        runrcg(filename, verbose=verbose, binpath=binpath)
        runrac(filename, verbose=verbose, binpath=binpath)
    elif 'bander' == calctype:
        if verbose:
            print('Executing ttban (',bandertype,') only')
        runban(filename, bandertype, verbose=verbose, binpath=binpath)
    else:
        raise ValueError('unrecognised calctype')

def runrcg(filename, verbose=False, binpath=None):
    """
    runrac expects to have
    <filename>.rcg
    in the working directory

    and produces
    <filename>.m14
    <filename>.org
    """

    #FNULL = open(devnull, 'w')
    if binpath is None:
        binpath=getBin()
    if exists('fort.10'):
        remove('fort.10')
    if isunix:
    #copy(filename + '.rcg','fort.10')
        link(filename + '.rcg', 'fort.10')
        if not exists('fort.72'):
            copy(join(binpath,'rcg_cfp72'),'fort.72')
        if not exists('fort.73'):
            copy(join(binpath,'rcg_cfp73'),'fort.73')
        if not exists('fort.74'):
            copy(join(binpath,'rcg_cfp74'),'fort.74')
        if verbose:
            print("Running ttrcg")
        p = system(join(binpath,'ttrcg') + ' >ttrcg.log 2>&1')
        try:
            move('fort.14',cat(filename,'.m14'))
        except:
            print("No fort.14")
        move('fort.9',cat(filename,'.org'))
        try:
            remove('fort.10')
        except:
            print("no fort.10")
    elif ispc:
        if exists('FTN10'):
            remove('FTN10')
        copy(cat(filename,'.rcg'), 'FTN10')
        p = system(' '.join(['1>NUL ',join(binpath,'rcg9.exe')]))
        move('FTN14',cat(filename,'.m14'))
        move('FTN09',cat(filename,'.org'))
        remove('FTN10')
    #os.symlink(join(binpath,'rcg_cfp72'),'fort.72')
    #os.symlink(join(binpath,'rcg_cfp73'),'fort.73')
    #os.symlink(join(binpath,'rcg_cfp74'),'fort.74')
       # map(remove, ['fort.72','fort.73','fort.74','fort.10'])
    #FNULL.close()

def runrac(filename, verbose=False, binpath=None):
    """
    runrac expects to have
    <filename>.rac
    <filename>.m14
    in the working directory

    and produces
    <filename>.m15
    <filename>.ora
    """

    #FNULL = open(devnull, 'w')
    if binpath is None:
        binpath=getBin()
    if isunix:
        if verbose:
            print("Running ttrac")
        p = system(' '.join([join(binpath,'ttrac'), filename + '.m14', filename + '.ora', '<', filename + '.rac', '>ttrac.log 2>&1']))
    #else:
    #    p = Popen([join(binpath,'ttrac'),cat(filename,'.m14'),cat(filename,'.ora')], stdin=open(cat(filename,'.rac'), 'r'), stdout=FNULL, stderr=FNULL)
    #p.wait()
        if exists('rme_out.dat'):
            move('rme_out.dat', filename + '.m15')
    #FNULL.close()
    elif ispc:
        system(cat('1>NUL ',join(binpath,'racer.exe '),filename,'.m14 ',filename,'.ora <',filename,'.rac'))
        if exists('rmeout'):
            move('rmeout',cat(filename,'.m15'))

def runban(filename, bandertype='lanczos', verbose=False, binpath=None):
    """
    runban expects to have
    <filename>.m14
    <filename>.m15
    <filename>.ban
    in the working directory

    and produces
    <filename>.oba
    """
    #FNULL = open(devnull, 'w')
    if binpath is None:
        binpath=getBin()
    if exists(filename + '.m15'): 
        move(filename + '.m15','FTN15')
    if exists(filename + '.m14'):
        move(filename + '.m14','FTN14')
    if exists('fort.50'):
        remove('fort.50')
    if isunix:
        link(filename + '.ban','fort.50')
    elif ispc:
        copy(filename + '.ban','fort.50')
    if 'lanczos' == bandertype:
        if verbose:
            print("Running ttban")
        #    p = Popen([join(binpath,'ttban')])
        if isunix:
            status = system(join(binpath, 'ttban') + ' >ttban.log 2>&1')
        elif ispc:
            status = system(''.join(['1>NUL ',binpath,'ander']))
        #else:
        #    p = Popen([join(binpath,'ttban')], stdout=FNULL, sterr=FNULL)
        #p.wait()
    elif 'exact' == bandertype:
        if verbose:
            print("Running ttban_exact")
            #p = Popen([join(binpath,'ttban_exact')])
        if isunix:
            status = system(join(binpath, 'ttban_exact')+ ' >ttban_exact.log 2>&1')
        elif ispc:
            status = system(join('1>NUL ',binpath,'Bander_Exact_MoreMem_static.exe'))
        #else:
        #    p = Popen([join(binpath,'ttban_exact')], stdout=FNULL, stderr=FNULL)
        #p.wait()
    move('FTN14', filename + '.m14')
    move('FTN15', filename + '.m15')
    move('fort.44', filename + '.oba')
    map(remove,['FTN98','FTN99','fort.50','fort.43'])
    #FNULL.close()

def runrcn(filestub, element, trans, verbose=False, binpath=None):
    filename = filestub + '.rcn'
    #CalcRCN(filename,element,trans)
    # filestub - any name without extention
    # element - 'Ti' 'V' 'Cr' 'Mn' 'Fe' 'Co' 'Ni' 'Cu' 'Zn'
    # trans - list of one or two configurations. Shells expressed with 4
    # characters. A single space seperates shell configurations for each state.
    # i.e one trans = ['2P06 3D05','2P05 3D06'] or a single config = ['1S01 2P06 3D05']
    # For a transition the ground and final state Confs need to be passed as an dict with keys ground and final.
    # For a single state just a dict with the ground key 
    # Michael L. Baker, translated to python by Charles J. Titus
    
    # Has not been set up or tested for PC, but could be in theory!
    #if isunix:
        # personal path here:
    if binpath is None:
        binpath= getBin()
    if isunix:
        Ipath=join(getenv('ttmult'),'inputs')
    elif ispc:
        Ipath='C:\\cowan\\inputs\\'
    elements=['Ti','V','Cr','Mn','Fe','Co','Ni','Cu','Zn']
    atomic_number=22
    check=0

    for ii in range(len(elements)):
        if elements[ii].lower() == element.lower():
            check=1
            break
        atomic_number=atomic_number + 1
    
    if check == 0:
        print('Choose from the following elements:')
        print(elements)
        raise ValueError('error in name given for element')
    
    
    # I never found any explanation for the meaning of the values in the
    # headerline. I only know that they are 'correct' for XAS simulations. I have
    # varified that these values are used in all informal documentation
    # available online and from random papers scavenged from the office. 
    # See https://www.tcd.ie/Physics/people/Cormac.McGuinness/Cowan/ for more
    # info.
    headline= '22 -9    2   10  1.0    5.E-06    1.E-09-2   130   1.0  0.65  0.0 0.50 0.0  0.70\n'
    if len(trans) == 1:
        gs='   %d       state 1              %s         \n'%(atomic_number,trans['ground'])
        rcn=headline + gs + '   -1'
    elif len(trans) == 2:
        gs='   %d       state 1              %s         \n'%(atomic_number,trans['ground'])
        fs='   %d       state 2              %s         \n'%(atomic_number,trans['final'])
        rcn=headline + gs + fs + '   -1'
    else:
        raise ValueError('too many states')
    
    with open(filename, 'w') as f:
        f.write(rcn)


    if isunix:
        if exists('fort.10'):
            remove('fort.10')
        link(filename,'fort.10')
        status = system(join(binpath,'rcn31') + ' >rcn31.log 2>&1')
    elif ispc:
        if exists('FTN10'):
            remove('FTN10')
        copy(filename,'FTN10')
        status = system(cat('1>NUL ',join(binpath,'rcn31')))
    #FNULL = open(devnull, 'w')
    #if verbose:
    #    p = Popen([join(binpath,'rcn31')])
    #else:
    #    p = Popen([join(binpath,'rcn31')])#, stdout=FNULL, stderr=FNULL)

    #p = system('echo "test popen"')
    #p.wait()
    #status = p.returncode
        
    if status != 0:
        raise ValueError('rcn31 path perhaps not setup correctly: %s, error: %d'%(binpath, status))
    
    if isunix:
        copy('fort.9','rcn31.out')
        remove('fort.10')
        if not exists('rcn2.inp'):
            copy(join(Ipath,'rcn2.inp'), 'rcn2.inp')
        link('rcn2.inp','fort.10')
        #Quadrupole input file (generally not used):
        #copyfile([Ipath,'rcn2q.inp'],'fort.10');
    elif ispc:
        copy('FTN09','rcn31.out')
        copy(join(Ipath,'rcn2.inp'),'FTN10')
    """
    if verbose:
        p = Popen([join(binpath,'rcn2')])
    else:
        p = Popen([join(binpath,'rcn2')], stdout=FNULL, stderr=FNULL)
    """
    status = system(join(binpath, 'rcn2') + ' >rcn2.log 2>&1')
    #p.wait()
    #status = p.returncode
    #FNULL.close()
    
    if status != 0:
        raise ValueError('rcn2 path perhaps not setup correctly')
    
    if isunix:
        move('fort.11',filestub + '.rcf')
        move('fort.9',filestub + '.09')
        remove('FTN02')
        remove('fort.10')
    elif ispc:
        move('FTN11', filestub + '.rcf')
        move('FTN09', filestub + '.09')
        remove('FTN02')
        remove('FTN10')


"""
Old code used for executing using windows binaries. When we have time, runrac, runrcg, and 
runban can be rewritten to auto-detect the OS and properly call the windows .exe files

        else:
            if ispc:
                ############# WINDOWS ################
                binpath='c:\\cowan\\bin\\'
# ttmult.m:101
                if 'all' == calctype:
                    disp(cat('Executing rcg9.exe, racer.exe before bander (',bandertype,')'))
                    copy(cat(filename,'.rcg'),'FTN10')
                    system(cat('1>NUL ',binpath,'rcg9.exe'))
                    move('FTN14',cat(filename,'.m14'))
                    move('FTN09',cat(filename,'.org'))
                    system(cat('1>NUL ',binpath,'racer.exe ',filename,'.m14 ',filename,'.ora <',filename,'.rac'))
                    move('rmeout',cat(filename,'.m15'))
                    copy(cat(filename,'.ban'),'fort.50')
                    move(cat(filename,'.m14'),'FTN14')
                    move(cat(filename,'.m15'),'FTN15')
                    if 'exact' == lower(bandertype):
                        dos(cat('1>NUL ',binpath,'Bander_Exact_MoreMem_static.exe'))
                    else:
                        if 'lanczos' == lower(bandertype):
                            dos(cat('1>NUL ',binpath,'ander'))
                    move('fort.44',cat(filename,'.oba'))
                    move('FTN14',cat(filename,'.m14'))
                    move('FTN15',cat(filename,'.m15'))
                    remove('fort.*')
                    remove('FTN*')
                else:
                    if cellarray(['rcg_racer','rcg_rac']) == calctype:
                        disp('Executing rcg9.exe, racer.exe only')
                        copy(cat(filename,'.rcg'),'FTN10')
                        system(cat('1>NUL ',binpath,'rcg9.exe'))
                        move('FTN14',cat(filename,'.m14'))
                        move('FTN09',cat(filename,'.org'))
                        system(cat('1>NUL ',binpath,'racer.exe ',filename,'.m14 ',filename,'.ora <',filename,'.rac'))
                        if exist('rmeout','file') == 2:
                            move('rmeout',cat(filename,'.m15'))
                    else:
                        if 'bander' == calctype:
                            #bander
                            disp(cat('Executing bander (',bandertype,') only.'))
                            copy(cat(filename,'.ban'),'fort.50')
                            move(cat(filename,'.m14'),'FTN14')
                            move(cat(filename,'.m15'),'FTN15')
                            if 'exact' == lower(bandertype):
                                dos(cat('1>NUL ',binpath,'Bander_Exact_MoreMem_static.exe'))
                            else:
                                if 'lanczos' == lower(bandertype):
                                    dos(cat('1>NUL ',binpath,'ander'))
                            if exist(cat(filename,'.oba'),'file') == 2:
                                remove(cat(filename,'.oba'))
                            move('fort.44',cat(filename,'.oba'))
                            move('FTN14',cat(filename,'.m14'))
                            move('FTN15',cat(filename,'.m15'))
                            remove('fort.*')
                            remove('FTN*')
                        else:
                            error('unrecognised calctype')
    
"""
