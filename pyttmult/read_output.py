from __future__ import print_function, division
import numpy as np
import time
from os.path import exists

def read_output(filename, square_trans=False, verbose=False):
    """
    Reads either oba or ora, depending on file ending. Returns dictionary of
    bras, kets, transition matrices, keyed by actor and then triad
    """
    mat_dict = {}
    bra_dict = {}
    ket_dict = {}

    if filename[-3:] == 'oba':
        read_matrix = read_oba_matrix
    elif filename[-3:] in ['out','oqa','ora']:
        read_matrix = read_ora_matrix
    elif exists(filename + '.oba'):
        filename = filename + '.oba'
        read_matrix = read_oba_matrix
    elif exists(filename + '.ora'):
        filename = filename + '.ora'
        read_matrix = read_ora_matrix
    else:
        print("Unrecognized filename: %s"%filename)
        return None
    
    with open(filename, 'r') as f:
        line = f.readline()
        while line != "":
            if 'TRANSFORMED' in line:
                xdim, ydim, actor, sym = parse_mat_header(line)
                bras, kets, mat = read_matrix(f, xdim, ydim)
                if len(sym) == 4 and sym[-1] == '0':
                    sym = tuple(sym[:-1])
                else:
                    sym = tuple(sym)
                if square_trans:
                    mat = mat**2
                if actor in mat_dict:
                    if sym in mat_dict[actor]:
                        oldmat = mat_dict[actor][sym]
                        mat_dict[actor][sym] = np.vstack([oldmat, mat])
                    else:
                        mat_dict[actor][sym] = mat
                else:
                    mat_dict[actor] = {sym: mat}
                if actor in bra_dict:
                    if sym in bra_dict[actor]:
                        oldbras = bra_dict[actor][sym]
                        bra_dict[actor][sym] = np.hstack([oldbras, bras])
                    else:
                        bra_dict[actor][sym] = bras
                else:
                    bra_dict[actor] = {sym: bras}
                if actor in ket_dict:
                    ket_dict[actor][sym] = kets
                else:
                    ket_dict[actor] = {sym: kets}
            line = f.readline()
    return bra_dict, ket_dict, mat_dict

def read_oba_matrix(f, xdim, ydim):
    if xdim > 1:
        print("Unexpected oba formatting, xdim(%d) > 1"%xdim)
    kets = []
    bras = []
    mat = []
    while len(kets) < ydim:
        bras, k, m = read_oba_submatrix(f)
        kets += k
        mat.append(m)
    return np.array(bras, dtype=np.float), np.array(kets, dtype=np.float), np.hstack(mat)
    
def read_ora_matrix(f, xdim, ydim):
    kets = []
    bras = []
    mat = []
    while len(kets) < ydim:
        bras, k, m = read_ora_submatrix(f, xdim)
        kets += k
        mat.append(m)
    return np.array(bras, dtype=np.float), np.array(kets, dtype=np.float), np.hstack(mat)

def read_oba_submatrix(f):
    line = f.readline()
    while "BRA/KET" not in line:
        line = f.readline()
    kets = line.split(':')[-1].split()
    bras = []
    mat = []
    b, r = f.readline().split(':')
    bras.append(b)
    mat.append(np.fromstring(r, sep=" "))
    return bras, kets, np.array(mat)
    
def read_ora_submatrix(f, dim):
    line = f.readline()
    while "BRA/KET" not in line:
        line = f.readline()
    kets = line.split(':')[-1].split()
    f.readline()
    bras = []
    mat = []
    for n in range(dim):
        b, r = f.readline().split(':')
        bras.append(b)
        mat.append(np.fromstring(r, sep=" "))
    return bras, kets, np.array(mat)
        

def parse_mat_header(line):
    """
    Parse ACTOR line, which is the same between .oba and .ora files
    """
    indexstar=line.index('*')
    indexclose=line.index(')')
    indexclose2=line.index(')', indexstar)
    indexopen=line.index('(')
    indexopen2 =line.index('(', indexclose)
    indexactor=line.index('ACTOR')
    sym=line[indexopen+1:indexclose].split()
    xdim=int(line[indexopen2 + 1:indexstar])
    ydim=int(line[indexstar + 1:indexclose2])
    actor=line[indexactor + 5:].lstrip().rstrip()
    return xdim, ydim, actor, sym


#------------------------OLD LEGACY CODE--------------------------

def old_read_output(filename, square_trans=False, verbose=False):
    """
    Read output file, and return the standard data
    """
    if 'oba' == filename[-3:]:
        return old_read_oba(filename, square_trans, verbose)
    elif filename[-3:] in ['out','oqa','ora']:
        return old_read_ora(filename, square_trans=square_trans, verbose=verbose)
    elif exists(filename + '.oba'):
        return old_read_oba(filename + '.oba', square_trans, verbose)
    elif exists(filename + '.ora'):
        return old_read_ora(filename + '.ora', square_trans=square_trans, verbose=verbose)
    else:
        return None


def old_read_oba(filename=None, square_trans=False, verbose=False):
    """
    read_oba(filename,bandertype)
    Bander outputs pre-squared Mtrans when run with lanczos. When running with exact,
    you may want to square Mtrans here. But you may not want to as well...
    transition matrix elements.
    [allEground,allEfinal,allMtrans,alltranssymg,alltranssymo,alltranssymf,header] = get_oba(filename,bandertype)
    read oba file return contents in a bunch of seperate arrays
    Discussion of performace testing found in line.
    Michael L. Baker, Charles Titus

    Currently not returning weights, triad_GS_energy
    """

    if '.oba' not in filename:
        filename += '.oba'
    fid=open(filename,'r')
    
    numcol = 7
    matrix_counter=0
    header_counter=0
    weights_counter=0
    tline = fid.readline()
    triad_GS_energy = []
    weights = []
    allEfinal = []
    allEground = []
    allMtrans = []
    alltranssymg = []
    alltranssymo = []
    alltranssymf = []
    allydim = []
    header = []

    while tline != "":
        while ' TRANSFORMED MATRIX FOR TRIAD' not in tline:
            if ' Ground state energy Eg0= ' in tline:
                triad_GS_energy.append(float(tline[26:38]))
                tline= fid.readline()
                if tline == '':
                    break
                weights.append(map(float, tline[52:76].split()))
                weights_counter=weights_counter + 1
            tline=fid.readline()
            if tline == '':
                break
            if ' Total' not in tline:
                header.append(tline)
                header_counter=header_counter + 1
        if tline == '':
            break
        #TRANSFORMED MATRIX FOR TRIAD...
        xdim, ydim, actor, sym = parse_mat_header(tline)
        Efinal = np.zeros(ydim)
        Mtrans = np.zeros(ydim)
        if ydim >= numcol:
            for full_lines in range(0, (ydim // numcol) * numcol, numcol):
                tline=fid.readline()
                Efinal[full_lines:full_lines+numcol] = np.fromstring(tline[11:-1],sep=' ', count=numcol)
                #Efinal[full_lines:full_lines+numcol] = np.fromstring(tline[11:-1],sep=' ')
                tline=fid.readline()
                Mtrans[full_lines:full_lines+numcol] = np.fromstring(tline[11:-1],sep=' ', count=numcol)
                #Mtrans[full_lines:full_lines+numcol] = np.fromstring(tline[11:-1],sep=' ')
            full_lines += numcol
        else:
            full_lines = 0
            
        if ydim%numcol != 0:
            tline=fid.readline()
            tok = tline[11:-1].split()
            for n in range(len(tok)):
                Efinal[full_lines+n] = float(tok[n])
            tline=fid.readline()
            tok = tline[11:-1].split()
            for n in range(len(tok)):
                Mtrans[full_lines+n] = float(tok[n])
            Eground=float(tline[:9])
        else:
            Eground=float(tline[:9])
        if square_trans:
            if verbose:
                print('squaring matrices')
            Mtrans= Mtrans ** 2

        allEfinal.append(Efinal)
        allEground.append(Eground)
        allMtrans.append(Mtrans)
        alltranssymg.append(sym[0])
        alltranssymo.append(sym[1])
        alltranssymf.append(sym[2])
        allydim.append(ydim)
    
    fid.close()
    minEground_index=np.argmin(triad_GS_energy)
    minEground = triad_GS_energy[minEground_index]
    return (allEground, allEfinal, allMtrans, alltranssymg, alltranssymo, alltranssymf, header, allydim)

def older_read_ora(filename,intadd=False, verbose=False):
    """
    Note, Matrix elements are not squared, but they will get squared during
    getsticks
    """
    # __________input parameters__________________
    i=0
    # numcol=7; # use 7 for RGAss
    numcol=12
    # ____________________________________________
    if verbose:
        print('loading file...')
    with open(filename, 'r') as f:
        lines = f.readlines()

    comments = []
    for line in lines:
        if "ECHO:" in line:
            comments.append(line)

    t=-1    
    j=0
    for line in lines:
        if ('TRANSI' not in line and 'PRINTTRANS' not in line):
            j+=1
        else:
            break

    sizeChar = lines[j][-3] # That's the character they check...?
    if np.isreal(sizeChar):
        if int(sizeChar) != 0:
            transsizexalt = int(sizeChar)
    else:
        transsizexalt = None
    
    actor = []
    transtriad = []
    transsymg =[]
    transsymo = []
    transsymf = []
    transsizex = []
    transsizey = []
    Efinal = []
    Eground = []
    Mtrans = []

    while j + 7 < len(lines):
        j=j + 1
        while j < len(lines) and 'TRANSFORMED' not in lines[j]:
            j += 1 # Find Transformed, which heralds start of data
        if j < len(lines):
            t=t + 1
            line = lines[j].rstrip('\n')
            xdim, ydim, actor, sym = parse_mat_header(line)
            j += 4 # Skip to first BRA/KET line
            # transtriad(t)=str2num(lines{j}(31:base(1)-1)); # TRIAD number
            transtriad.append(t)
            transsymg.append(sym[0])
            transsymo.append(sym[1])
            transsymf.append(sym[2])
            if transsizexalt is not None:
                xdim = transsizexalt
            transsizex.append(xdim)
            transsizey.append(ydim)
            Efinal.append(np.zeros((1, ydim)))
            Eground.append(np.zeros((xdim, 1)))
            Mtrans.append(np.zeros((xdim, ydim)))
            for p in range(int(np.ceil(ydim*1.0 / numcol))):
                EIdxStart = numcol*p
                EIdxEnd = min(numcol + numcol*p,ydim)
                EfinalLine = lines[j] # BRA/KET line
                Efinal[t][0, EIdxStart:EIdxEnd]=np.fromstring(EfinalLine[12:-1], sep=' ')
                j += 2 # Skip to beginning of Matrix elements
                Vtemp = np.fromstring(''.join(lines[j:j+xdim]).replace('\n', ' ').replace(':', ''), sep=' ')
                try:
                    Mtemp=Vtemp.reshape(xdim, len(Vtemp)/xdim)
                except:
                    print(p, j, np.ceil(ydim / numcol), xdim, ydim, len(Vtemp))
                    print(Vtemp)
                    raise
                Mtrans[t][:,EIdxStart:EIdxEnd] = Mtemp[:,1:]
                if p == 0:
                    Eground[t]=Mtemp[:,0]
                j += xdim + 1 # Next BRA/KET line
            # round to 0.1 meV -> eliminate numerical error in RACAH
            Eground[t]=np.round(Eground[t]*10000) / 10000.0
            Efinal[t]=np.round(Efinal[t]*10000) / 10000.0

    if intadd:
        raise NotImplementedError
        Mtrans,Eground,Efinal=stickcombine_st(Mtrans,Eground,Efinal,intadd,nargout=3)


    allEground = []
    allEfinal = []
    allMtrans = []
    for ii in range(len(Eground)):
        allEgroundmat=np.tile(Eground[ii][..., None],(1,Efinal[ii].size))
        allEfinalmat=np.tile(Efinal[ii],(Eground[ii].size,1))
        allEground.append(allEgroundmat.flatten())
        allEfinal.append(allEfinalmat.flatten())
        allMtrans.append(Mtrans[ii].flatten())
    #return Eground, Efinal, Mtrans, transsymg, transsymo, transsymf, transtriad, actor
    return (allEground,allEfinal,allMtrans,transsymg,transsymo,transsymf,transtriad,actor)


def old_read_ora(filename, intadd=False,  square_trans=True, verbose=False):
    """
    Reads ora file and returns a list of flattened ground state energies,
    final state energies, and transition matrices for each triad
    """
    actors = []
    transtriad = []
    transsymg =[]
    transsymo = []
    transsymf = []
    transsizex = []
    transsizey = []
    Efinal = []
    Eground = []
    Mtrans = []

    if '.ora' not in filename:
        filename += '.ora'
        
    with open(filename, 'r') as f:
        line = f.readline()
        while line != "":
            if "TRANSFORMED" in line:
                xdim, ydim, actor, sym = parse_mat_header(line)
                bras, kets, mat = read_ora_matrix(f, xdim, ydim)
                actors.append(actor)
                transtriad.append(sym)
                transsymg.append(sym[0])
                transsymo.append(sym[1])
                transsymf.append(sym[2])
                Efinal.append(kets)
                Eground.append(bras)
                if square_trans:
                    Mtrans.append(mat**2)
                else:
                    Mtrans.append(mat)
            line = f.readline()

    allEground = []
    allEfinal = []
    allMtrans = []
    for ii in range(len(Eground)):
        allEfinalmat, allEgroundmat = np.meshgrid(Efinal[ii], Eground[ii])
        allEground.append(allEgroundmat.flatten())
        allEfinal.append(allEfinalmat.flatten())
        allMtrans.append(Mtrans[ii].flatten())

    return allEground, allEfinal, allMtrans, transsymg, transsymo, transsymf, transtriad, actor


